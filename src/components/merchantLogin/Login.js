import React, { useEffect, useState } from 'react'
import clsx from 'clsx'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import routeConfig from '../../routing/routeConfig'
import { LoginActions } from '../../store/actions'
import { USER_ERROR, REGEX, RESPONSE_STATUS, SESSION } from '../../utils/constants'
import _ from 'lodash'
import { setLocalStorage } from '../../utils/helper'

/*
  Formik+YUP+Typescript:
  https://jaredpalmer.com/formik/docs/tutorial#getfieldprops
  https://medium.com/@maurice.de.beijer/yup-validation-and-typescript-and-formik-6c342578a20e
*/

function MerchantLogin(props) {
  const { loginDispatch, loading, loginData, clearLogin } = props
  const [formData, setFormData] = useState({
    email: '',
    phone: '',
    address: '',
    ipAddress: '',
    deviceID: ''
  })
  const [errors, setErrors] = useState({
    email: '',
    phone: '',
    address: '',
    ipAddress: '',
    deviceID: ''
  })
  const [showBanner, setShowBanner] = useState(false)
  const [show, setShow] = useState(false)

  const handleSubmit = (e) => {
    const errors = {}
    if (_.isEmpty(formData.email)) {
      errors.email = USER_ERROR.EMAIL_REQUIRED
    } else if (formData.email && !REGEX.EMAIL.test(formData.email)) {
      errors.email = USER_ERROR.EMAIL_INVALID
    }
    if (_.isEmpty(formData.phone)) {
      errors.phone = USER_ERROR.PHONE
    }
    if (_.isEmpty(formData.address)) {
      errors.address = USER_ERROR.ADDRESS
    }
    if (_.isEmpty(formData.ipAddress)) {
      errors.ipAddress = USER_ERROR.IP_ADDRESS
    }
    if (_.isEmpty(formData.deviceID)) {
      errors.deviceID = USER_ERROR.DEVICE
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      loginDispatch(formData)
    }
  }

  const handleChange = (e) => {
    const { name, value } = e.target
    formData[name] = value
    setFormData(formData)
    setErrors({ ...errors, [name]: '' })
  }
  useEffect(() => {
    if ( loginData.status === RESPONSE_STATUS.SUCCESS) {
  clearLogin()
      setLocalStorage(SESSION.TOKEN, loginData.data.token)
      setShow(true)
    } else if (loginData.status === RESPONSE_STATUS.ERROR) {
      setShowBanner(true)
      setTimeout(() => {
        setShowBanner(false)
        clearLogin()
      }, 3000)
    }
  }, [loginData])

  const showSubmit = () => {
    setShow(false)
    clearLogin()
    setFormData({
      email: '',
      phone: '',
      address: '',
      ipAddress: '',
      deviceID: ''
    })
  }

  return (
    <>
      {/* begin::Banner */}
      {showBanner &&
        <div className='mb-10 bg-light-info p-8 rounded'>
          <div className='text-center text-danger'>
            {loginData.message}
          </div>
        </div>}
      {/* end::Banner */}
{
  !show ?
  (
<> 
   {/* begin::Heading */}
   <div className='text-center mb-10'>
        <h1 className='text-dark mb-3'>Risk Analytics</h1>
      </div>
      {/* end::Heading */}

 {/* begin::Form group */}
 <div className='fv-row mb-10'>
     <label className='form-label fs-6 fw-bolder text-dark'>Phone Number</label>
     <input
       placeholder='Phone Number'
       className={clsx(
         'form-control form-control-lg form-control-solid',
         { 'is-invalid': formData.phone && errors.phone },
         {
           'is-valid': formData.phone && !errors.phone
         }
       )}
       onChange={(e) => handleChange(e)}
       onKeyPress={(e) => {
        if (!/[0-9]/.test(e.key)) {
          e.preventDefault()
        }
      }}
       type='phone'
       name='phone'
       autoComplete='off'
       maxLength={10}
     />
     {errors.phone && (
       <div className='fv-plugins-message-container text-danger'>
         <span role='alert text-danger'>{errors.phone}</span>
       </div>
     )}
   </div>
   <div className='fv-row mb-10'>
     <label className='form-label fs-6 fw-bolder text-dark'>Email ID</label>
     <input
       placeholder='Email'
       className={clsx(
         'form-control form-control-lg form-control-solid',
         { 'is-invalid': formData.email && errors.email },
         {
           'is-valid': formData.email && !errors.email
         }
       )}
       onChange={(e) => handleChange(e)}
       type='email'
       name='email'
       autoComplete='off'
     />
     {errors.email && (
       <div className='fv-plugins-message-container text-danger'>
         <span role='alert text-danger'>{errors.email}</span>
       </div>
     )}
   </div>
   <div className='fv-row mb-10'>
     <label className='form-label fs-6 fw-bolder text-dark'>Address</label>
     <textarea
       placeholder='Address'
       className={clsx(
         'form-control form-control-lg form-control-solid',
         { 'is-invalid': formData.address  && errors.address  },
         {
           'is-valid': formData.address  && !errors.address 
         }
       )}
       onChange={(e) => handleChange(e)}
       type='address'
       name='address'
       autoComplete='off'
     />
     {errors.address  && (
       <div className='fv-plugins-message-container text-danger'>
         <span role='alert text-danger'>{errors.address }</span>
       </div>
     )}
   </div>
   <div className='fv-row mb-10'>
     <label className='form-label fs-6 fw-bolder text-dark'>IP Address</label>
     <textarea
       placeholder='Ip Address'
       className={clsx(
         'form-control form-control-lg form-control-solid',
         { 'is-invalid': formData.ipAddress  && errors.ipAddress  },
         {
           'is-valid': formData.ipAddress  && !errors.ipAddress 
         }
       )}
       onChange={(e) => handleChange(e)}
       type='ipAddress'
       name='ipAddress'
       autoComplete='off'
       onKeyPress={(e) => {
        if (!/[0-9{1-3}.]/.test(e.key)) {
          e.preventDefault()
        }
      }}
     />
     {errors.ipAddress  && (
       <div className='fv-plugins-message-container text-danger'>
         <span role='alert text-danger'>{errors.ipAddress }</span>
       </div>
     )}
   </div>
   <div className='fv-row mb-10'>
     <label className='form-label fs-6 fw-bolder text-dark'>Device ID</label>
     <input
       placeholder='Device ID'
       className={clsx(
         'form-control form-control-lg form-control-solid',
         { 'is-invalid': formData.deviceID  && errors.deviceID  },
         {
           'is-valid': formData.deviceID  && !errors.deviceID 
         }
       )}
       onChange={(e) => handleChange(e)}
       type='deviceID'
       name='deviceID'
       autoComplete='off'
     />
     {errors.deviceID  && (
       <div className='fv-plugins-message-container text-danger'>
         <span role='alert text-danger'>{errors.deviceID }</span>
       </div>
     )}
   </div>
   
   {/* end::Form group */ }

  {/* begin::Action */ }
  <div className='text-center'>
    <button
      type='button'
      id='kt_sign_in_submit'
      className='btn btn-lg btn-primary w-100 mb-5'
      onClick={(e) => handleSubmit(e)}
      disabled={loading}
    >
      {!loading && <span className='indicator-label'>Submit</span>}
      {loading && (
        <span className='indicator-progress' style={{ display: 'block' }}>
          Please wait...
          <span className='spinner-border spinner-border-sm align-middle ms-2' />
        </span>
      )}
    </button>
  </div>
  {/* end::Action */ }

</>
    

  ): (
    <> 
      <div className='bg-light-info rounded'>
          <div className='text-center text-success fw-bolder fs-3 mb-4'>
            Thanks For Submitting The Request. We Are Processing your Request.
          </div>
          <div className='text-center'>
            <div className='row'>
            <div className='col-sm-4 col-md-4 col-lg-4'/>

              <div className='col-sm-3 col-md-3 col-lg-3'>

              <button
      type='button'
      id='kt_sign_in_submit'
      className='btn btn-sm btn-info w-100'
      onClick={() => showSubmit()}
      disabled={loading}
    >
      {!loading && <span className='indicator-label'>Back</span>}
      {loading && (
        <span className='indicator-progress' style={{ display: 'block' }}>
          Please wait...
          <span className='spinner-border spinner-border-sm align-middle ms-2' />
        </span>
      )}
    </button>
              </div>
            </div>
  </div>
        </div>
    </>
  )
}

     
    </>
  )
}

const mapStateToProps = state => {
  const { loginStore } = state
  return {
    loginData: loginStore && loginStore.login ? loginStore.login : {},
    loading: loginStore && loginStore.loading ? loginStore.loading : false
  }
}

const mapDispatchToProps = dispatch => ({
  loginDispatch: (data) => dispatch(LoginActions.login(data)),
  clearLogin: () => dispatch(LoginActions.clearLogin())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MerchantLogin)
