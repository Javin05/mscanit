import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import _, { result } from 'lodash'
import { KTSVG } from '../../theme/helpers'
import { riskManagementActions } from '../../store/actions'
import { connect } from 'react-redux'
import SearchList from './searchList'
import ReactPaginate from 'react-paginate'
import axios from 'axios'

function RiskManagementList(props) {
  const {
    getRiskManagementlistDispatch,
    className,
    riskmgmtlists,
    loading
  } = props
  const [limit, setLimit] = useState(25)
  const [ data, setData] = useState({})
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [datas,setDatas] = useState([]);


  const [sorting, setSorting] = useState({
    deviceID: false,
    phone: false,
    email: false,
    ipAddress: false,
    address: false,
    status: false
  })

  useEffect(()=>{
    axios.get(`https://jsonplaceholder.typicode.com/users`)
    .then(result => setDatas(result.data))
    .catch(err => console.log(err))
  })

  useEffect(() => {
    const params = {
      limit: limit,
      page: 1
    }
    getRiskManagementlistDispatch(params)
  }, [])

  useEffect(() => {
    const params = {
      limit: limit,
      page: 1
    }
    getRiskManagementlistDispatch(params)
  }, [limit])

  const handleRecordPerPage = (e) => {
    const { value } = e.target
    setLimit(value)
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber
    }
    setActivePageNumber(pageNumber)
    getRiskManagementlistDispatch(params)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC'
      }
      getRiskManagementlistDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC'
      }
      getRiskManagementlistDispatch(params)
    }
  }

  const totalPages =
    riskmgmtlists && riskmgmtlists.count
      ? Math.ceil(parseInt(riskmgmtlists && riskmgmtlists.count) / limit)
      : 1

  return (
    <>
      <div className={`card ${className}`}>
        <div className='card-body py-3'>
          <div className='d-flex  px - 2'>
            <div className='d-flex justify-content-start col-md-6'>
              <div className='col-md-3 mt-1'>
                {riskmgmtlists && riskmgmtlists.count && (
                  <span className='text-muted fw-bold d-flex fs-3 mt-2'>
                    Total: &nbsp;{' '}
                    <span className='text-gray-700 fw-bolder text-hover-primary fs-3'>
                      {riskmgmtlists.count}
                    </span>
                  </span>
                )}
              </div>
              <div className='col-md-7 d-flex'>
                <label className='col-form-label text-lg-start'>
                  Record(s) per Page : &nbsp;{' '}
                </label>
                <div className='col-md-3'>
                  <select
                    className='form-select w-6rem'
                    data-control='select'
                    data-placeholder='Select an option'
                    data-allow-clear='true'
                    onChange={(e) => handleRecordPerPage(e)}
                  >
                    <option value='25'>25</option>
                    <option value='50'>50</option>
                    <option value='75'>75</option>
                    <option value='100'>100</option>
                  </select>
                </div>
              </div>
            </div>
            <div className='d-flex col-md-6 justify-content-end my-auto'>
              <div className="my-auto">
                <SearchList />
              </div>
              <div className='my-auto me-3'>
                <Link
                  to='#'
                  className='btn btn-sm btn-light-primary btn-responsive font-5vw'
                >
                  {/* eslint-disable */}
                  <KTSVG path="/media/icons/duotune/files/fil003.svg" />
                  {/* eslint-disable */}
                  Add Merchant
                </Link>
              </div>
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead>
                <tr className="fw-bold fs-6 text-gray-800">
                  <th className="min-w-200px text-start">
                    <div className="d-flex">
                      <span>Device ID</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("deviceID")}
                        >
                          <i
                            className={`bi ${sorting.deviceID
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}

                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-200px text-start">
                    <div className="d-flex">
                      <span>Individual Phone</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("phone")}
                        >
                          <i
                            className={`bi ${sorting.phone
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-200px text-start">
                    <div className="d-flex">
                      <span>Individual Email</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("email")}
                        >
                          <i
                            className={`bi ${sorting.email
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-200px text-start">
                    <div className="d-flex">
                      <span>IP Address</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("ipAddress")}
                        >
                          <i
                            className={`bi ${sorting.ipAddress
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-200px text-center">
                    <div className="d-flex">
                      <span>Address</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("address")}
                        >
                          <i
                            className={`bi ${sorting.address
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-200px text-center">
                    <div className="d-flex">
                      <span>Status</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("status")}
                        >
                          <i
                            className={`bi ${sorting.status
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody>
                {
                  !loading
                    ? (
                      riskmgmtlists &&
                        riskmgmtlists.data &&
                        riskmgmtlists.data
                        ? (
                          riskmgmtlists.data.map((riskmgmtlist, _id) => {
                            return (
                              <tr
                                // key={_id}
                                // style={
                                //   _id === 0
                                //     ? { borderColor: "black" }
                                //     : { borderColor: "white" }
                                // }
                                key= {datas.name}
                              >
                                <td className="pb-0 pt-5  text-start">
                                  <Link to={`/merchant/${riskmgmtlist._id}`} className="pb-0 pt-5  text-start">

                                    {riskmgmtlist.deviceID ? riskmgmtlist.deviceID : "--"}
                                  </Link>

                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  {/* {riskmgmtlist.phone ? riskmgmtlist.phone : "--"} */}
                                  {datas.name}
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  {riskmgmtlist.email ? riskmgmtlist.email : "--"}
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  {riskmgmtlist.ipAddress ? riskmgmtlist.ipAddress : "--"}
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  {riskmgmtlist.address ? riskmgmtlist.address : "--"}
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  {riskmgmtlist.status ? riskmgmtlist.status : "--"}
                                </td>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='6'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='6' className='text-center'>
                          {/* <div className='spinner-border text-primary m-5' role='status' /> */}
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div>
           
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  const { riskManagementlistStore } = state;
  return {
    riskmgmtlists:
      state &&
      state.riskManagementlistStore &&
      state.riskManagementlistStore.riskmgmtlists,
    loading:
      riskManagementlistStore && riskManagementlistStore.loading ? riskManagementlistStore.loading : false,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getRiskManagementlistDispatch: (params) =>
    dispatch(riskManagementActions.getRiskManagementlist(params))
});

export default connect(mapStateToProps, mapDispatchToProps)(RiskManagementList);
