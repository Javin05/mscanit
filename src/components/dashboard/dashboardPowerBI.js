import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import '../../styles/dashboard/posts.scss'
import axiosInstance from '../../services'

function DashboardPowerBI (props) {
  const [source, setSource] = useState()
  useEffect(() => {
    // props.getPostsDispatch()
    getUrl()
    return () => {
      props.clearPostsDispatch()
      setSource(null)
    }
  }, [])

  const getUrl = async () => {
    const url = 'https://chargebackzero-userservice.herokuapp.com/api/v1/dashboard'
    axiosInstance.get(url).then(
      (response) => {
        if (response.status === 200) {
          const src = response.data && response.data.data && response.data.data.EmbedUrl
            ? response.data.data.EmbedUrl
            : ''
          setSource(src)
        }
      }
    )
  }

  return (
    <div className='card p-0' style={{ height: '100vh' }}>
      <iframe
        // src='https://app.powerbi.com/view?r=eyJrIjoiMzk0MmEzODUtN2JkMS00ZjMzLWI0ZmYtZWU5ODJiODYxMTM1IiwidCI6IjE1M2RhMTU0LTY3NGMtNDViOS1hMWU1LWI0MGZhY2ZlOWU3MiJ9'
        src={source}
        width='100%'
        height='100%'
      />
    </div>
  )
}

const mapStateToProps = (state) => ({
  posts: state && state.postsStore && state.postsStore.posts
})

const mapDispatchToProps = (dispatch) => ({
  // getPostsDispatch: () => dispatch(postActions.getPosts()),
  // clearPostsDispatch: () => dispatch(postActions.clearPosts())
})

export default connect(mapStateToProps, mapDispatchToProps)(DashboardPowerBI)
