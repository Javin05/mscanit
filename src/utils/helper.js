export const setLocalStorage = (key, varToSet) => (localStorage.setItem(key, btoa(varToSet)))

export const getLocalStorage = (key) => {
  const getStorage = localStorage.getItem(key)
  if (localStorage.getItem("token")) {
    return true;
  }
  return false;
  // try {
  //   return getStorage ? atob(getStorage) : false
  // } catch (e) {
  //   return false
  // }
}

export const unsetLocalStorage = () => {
  localStorage.clear()
  console.log(localStorage)
}
