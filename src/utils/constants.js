export const RESTART_ON_REMOUNT = '@@saga-injector/restart-on-remount'
export const DAEMON = '@@saga-injector/daemon'
export const ONCE_TILL_UNMOUNT = '@@saga-injector/once-till-unmount'

export const colors = {
  oxfordBlue: '#181c32'
}

export const USER_ERROR = {
  COMPANYNAME: 'Company Name is required.',
  PRODUCTNAME: 'Product name is required.',
  MERCHANTNAME: 'Merchant Name is required.',
  DBA: 'DBA is required.',
  MERCHANT_ID: 'Merchant Id is required.',
  DISPUTE_METHOD: 'Dispute method is required.',
  DISPUTE_AMOUNT: 'Dispute amount is required.',
  DISPUTE_DATE: 'Dispute date is required.',
  CB_CASE: 'Chargeback case is required.',
  CB_DATE: 'Chargeback Date is required.',
  CB_DEADLINE: 'Chargeback Deadline is required.',
  CB_AMOUNT: 'Chargeback amount is required.',
  CB_STATUS: 'Chargeback status is required.',
  CB_TYPE: 'Chargeback Type is required.',
  CARD_SIX: 'Card first six number is required.',
  CARD_FOUR: 'Card last four digit is required.',
  CARD_TYPE: 'Card Type is required.',
  CB_REASON: 'Chargeback reason is required.',
  TD_DATE: 'Transaction Date is required.',
  TD_AMT: 'Transaction amount is required.',
  TD_NO: 'Transaction number is required.',
  SA_TY: 'Sale Type is required.',
  CUS_IP: 'Cutomer IP is required.',
  CUS_PH: 'Customer Phone is required.',
  AFF_ID: 'Affiliate ID is required.',
  AVS_MATCH: 'AVS is required.',
  CVV: 'CVV is required.',
  CUS_NA: 'Customer Name is required.',
  CUS_EMAIL: 'Customer email is required.',
  AUTH_CODE: 'Authentication code is required.',
  COUNTRY: 'Country is required. ',
  NON_US: 'Non US country is required.',
  MERCHANT_DESCRIPTOR: 'Merchant Descriptor is required.',
  ARN: 'ARN is required.',
  TRANSACTION_CODE: 'Transaction Code is required.',
  TRANSACTION_ID: 'Transaction ID is required.',
  TRANSACTION_AMOUNT: 'Transaction Amount is required.',
  TRANSACTION_CURRENCY: 'Transaction Currency is required.',
  AVS_CODE: 'AVS Code is required.',
  CVV_CODE: 'CVV Code is required.',
  SECURE_CODE: 'Secure Code is required.',
  RE_FUNDED: 'Refunded is required.',
  ISSUE_BANK: 'IssueBank is required.',
  CARD_COUNTRY: 'Card Country is required.',
  BILLINGF_NAME: 'Billing FirstName is required.',
  BILLINGL_NAME: 'Billing LastName is required.',
  BILLING_COUNTRY: 'Billing Country is required.',
  BILLING_STATE: 'Billing State is required.',
  BILLING_CITY: 'Billing City is required.',
  BILLING_ADDRESS: 'Billing Address is required.',
  BILLING_ZIPCODE: 'Billing ZipCode is required.',
  ACC_USERNAME: 'Account UserName is required.',
  EMAIL: 'Email is required.',
  PHONE_NUMBER: 'Phone Number is required.',
  ACC_ACTIVEDATE: 'Account Active Date is required.',
  PURCHASE_DATE: 'First Purchase Date is required.',
  LAST_LOGIN: 'Last Login is required.',
  IP_ADDRESS: 'IP Address is required.',
  IP_LOCATION: 'IP Location is required.',
  DEVICE_ID: 'Device ID is required.',
  DEVICE_NAME: 'Device Name is required.',
  LOGIN_HISTORY: 'Login History is required.',
  TRANSACTION_HISTORY: 'Transaction History is required.',
  PRODUCT_TYPE: 'Product Type is required.',
  PRODUCT_NAME: 'Product Name is required.',
  PRO_ACQ_DATE: 'Product Acquisation Date is required.',
  DELIVERY_DATE: 'Delivery Date is required.',
  TRACKING_NO: 'Tracking Number is required.',
  SHIPPING_COUNTRY: 'Shipping Country is required.',
  SHIPPING_STATE: 'Shipping State is required.',
  SHIPPING_CITY: 'Shipping City is required.',
  SHIPPING_ADDRESS: 'Shipping Address is required.',
  SHIPPING_ZIPCODE: 'Shipping ZipCode is required.',
  WEBSITE: 'Website is required.',
  DESCRIPTION: 'Description is required.',
  BENIFITS: 'Benifits is required.',
  SPECIFICINFO: 'Specific Info is required.',
  CONTACT_URL: 'Contact URL is required.',
  LIVE_CHAT: 'Support Live Chat is required.',
  TERMS_URL: 'Terms and Condition URL is required.',
  REFUND_URL: 'Refund Policy URL is required.',
  FILE: 'File is required. ',
  EMAIL_REQUIRED: 'Email address is required.',
  EMAIL_INVALID: 'Email address is invalid.',
  PASSWORD_REQUIRED: 'Password is required.',
  PASSWORD_MIN_MAX_LENGTH: 'Should be greater than 6 or less than 14 digits.',
  PASSWORD_INVALID: 'Please enter valid password.',
  CONFIRM_PASSWORD_REQUIRED: 'Confirm password is required.',
  PASSWORD_SAME: 'Password and Confirm password should be same.',
  ALERT_ID: 'Alert Id is required.',
  ALERT_DATE: 'Alert Date is required.',
  ALERT_TYPE: 'Alert Type is required.',
  TRNS_TYPE: 'Transaction Type is required.',
  UPI_ID: 'UPI Transaction ID is required.',
  CUS_FNAME: 'Customer First Name is required.',
  CARD_BIN: 'Card Bin is required. ',
  EXP_MONTH: 'Expiration Month is required.',
  EXP_YEAR: 'Expiration Year is required.',
  CUS_LNAME: 'Customer Last Name is required.',
  DES: 'Descriptor is required.',
  DES_CONTACT: 'Descriptor Contact is required.',
  DES_CONTACT_INVALID: 'Descriptor Contact is invalid.',
  NOT_TYPE: 'Notification Type is requirted.',
  BANK: 'Bank Selection is required.',
  CARD_SIX_INVALID: 'Card First Six digits is invalid.',
  CARD_FOUR_INVALID: 'Card Last Four digits is invalid.',
  PROVIDER: 'Provider is required. ',
  RES_DATE: 'Resolution Date is required.',
  DURATION: 'Duration is required.',
  MERCHANT: 'Merchant is required.',
  REFUND_STATUS: 'Refund Status is required.',
  REFUND_AMOUNT: 'Refund Amount is required.',
  REFUND_DATE: 'Refund Date is required.'
}

export const DATE = {
  TIME: 'Time',
  TIME_INTERVAL: 15,
  TWELVE_HRS_FORMAT: 'h:mm A',
  DATE_TIME_FORMAT: ' D/MM/YYYY, h:mm A',
  DATE_ONLY: 'DD/MM/YYYY',
  DATE_FOR_PICKER: 'yyyy/MM/dd',
  DATE_MONTH_ONLY: 'dd/MM',
  DATE_MONTH_ONLY__: 'DD/MM',
  MONTH_ONLY: 'MM',
  YEAR_ONLY: 'yyyy',
  EXAMPLE_DATE: '2017-03-13',
  DATE_FORMAT: 'YYYY-MM-DD',
  DATE_FORMAT_MINS: 'MMM-DD-YYYY, hh:mm',
  DATE_FOR_PICKER_MONTH: 'MM'
}

export const REGEX = {
  EMAIL: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/,
  DOB: /^\d{2}[-/.]\d{2}[-/.]\d{4}$/,
  PHONE: /^[0][1-9]\d{9}$|^[1-9]\d{9}$/,
  NUMERIC: /^[0-9]+$/,
  PASSWORD: /^[a-zA-Z0-9@#^\-_$]*$/,
  PASSWORD_MIN_MAX_LENGTH: /^[a-zA-Z0-9@#^\-_$]{6,14}$/,
  TEXT: /^[a-zA-Z_ ]*$/,
  ALPHA_NUMERIC: /^[a-zA-Z0-9 /]*$/,
  ALPHA_NUMERIC_CHARS: /^[a-zA-Z0-9@#^\-_$/]*$/,
  ALPHA_NUMERIC_CHARS_SPACE: /^[a-zA-Z0-9@^\-_.,àâçéèêëîïôûùüÿñæœ +g]*$/,
  ALPHA_CHARS_SPACE: /^[a-zA-Z^-_$.,àâçéèêëîïôûùüÿñæœ +g]*$/,
  ZIP_CODE: /^([0-9]){5,6}$/,
  NUMERIC_SIX_DIGITS: /^([0-9]){6}$/,
  NUMERIC_FOUR_DIGITS: /^([0-9]){4}$/,
  SPACE: / +/g
}

export const RESPONSE_STATUS = {
  SUCCESS: 'ok',
  ERROR: 'error'
}

export const SESSION = {
  TOKEN: 'token',
  EXPIRED: 'session_expired',
  RESET_TOKEN: 'resetToken'
}

export const ORDER_INSIGHT_LAYOUT = {
  EXPORT: 'exportPIselectedCols',
  SEARCH: 'searchPIselectedCols',
  TABLE_LAYOUT: 'tableLayoutPIselectedCols'
}

export const CHARGEBACK_LAYOUT = {
  EXPORT: 'exportCBselectedCols',
  SEARCH: 'searchCBselectedCols',
  TABLE_LAYOUT: 'tableLayoutCBselectedCols'
}

export const INQUIRY_ALERT_LAYOUT = {
  EXPORT: 'export',
  SEARCH: 'search',
  TABLE_LAYOUT: 'tableLayout'
}

export const LAYOUT_DETAILS = {
  EXPORT_IBA: 'exportIBAselectedCols',
  SEARCH_IBA: 'searchIBAselectedCols',
  TABLE_LAYOUT_IBA: 'tableLayoutIBAselectedCols'
}

export const PREVENTION_LAYOUT = {
  EXPORT: 'exportPAselectedCols',
  SEARCH: 'searchPAselectedCols',
  TABLE_LAYOUT: 'tableLayoutPAselectedCols'
}

export const ENV = {
  SDK_URL: process.env.REACT_APP_SDK_URL
}

export const STATUS_RESPONSE = {
  SUCCESS_MSG: 'ok',
  ERROR_MSG: 'error'
}

export const ALERT_TYPE = {
  SELECT: '',
  INQUIERY: 'Inquiry',
  NOTIFICATION: 'Notification'
}

export const MONTH = {
  SELECT: '',
  INQUIERY: 'Inquiry',
  NOTIFICATION: 'Notification'
}

export const NOTIFICATION_TYPE = {
  SELECT: '',
  BLANK_SPACE: 'Blank Space',
  CANCEL_REQURRING: 'Cancel Recurring',
  CHARGEBACK: 'Chargeback',
  EXCEPTION_UNKOWN: 'Exception or Unknown',
  FRAUD_ALERT: 'Fraud Alert'
}
