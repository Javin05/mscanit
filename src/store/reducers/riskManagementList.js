import { riskManagementActionsTypes } from '../actions'

export const riskManagementlistInitialState = {
  list: null
}

export const riskManagementlistStoreKey = 'riskManagementlistStore'

export const riskManagementlistReducer = (state = riskManagementlistInitialState, action) => {
  switch (action.type) {
    case riskManagementActionsTypes.GET_RISK_MANAGEMENT_LIST:
      return { ...state, loading: true }
    case riskManagementActionsTypes.SAVE_RISK_MANAGEMENT_LIST_RESPONSE:
      return { ...state, riskmgmtlists: action.data, loading: false }
    case riskManagementActionsTypes.CLEAR_RISK_MANAGEMENT_LIST:
      return { ...state, riskmgmtlists: null }
    default:
      return state
  }
}
