import {
    FETCH_OWNMARKET
} from '../constants/OwnmarketConstant';

export const FetchOwnmarket = () => {
    return {
        type: FETCH_OWNMARKET,
    }
};
