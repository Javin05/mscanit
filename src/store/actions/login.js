export const LoginActionsTypes = {
  LOGIN: 'LOGIN',
  SAVE_LOGIN_RESPONSE: 'SAVE_LOGIN_RESPONSE',
  CLEAR_LOGIN: 'CLEAR_LOGIN'
}

export const LoginActions = {
  login: (data) => {
    return {
      type: LoginActionsTypes.LOGIN,
      payload: data
    }
  },
  saveLoginResponse: data => ({
    type: LoginActionsTypes.SAVE_LOGIN_RESPONSE,
    data
  }),
  clearLogin: () => ({
    type: LoginActionsTypes.CLEAR_LOGIN
  })
}
