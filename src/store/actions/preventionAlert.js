export const preventionAlertActionsTypes = {
  GET_PREVENTIONALERT: 'GET_PREVENTIONALERT',
  SAVE_PREVENTIONALERT_RESPONSE: 'SAVE_PREVENTIONALERT_RESPONSE',
  CLEAR_PREVENTIONALERT: 'CLEAR_PREVENTIONALERT'
}
export const preventionAlertActions = {
  getPreventionAlert: (params) => ({
    type: preventionAlertActionsTypes.GET_PREVENTIONALERT,
    params
  }),
  savePreventionAlertResponse: data => ({
    type: preventionAlertActionsTypes.SAVE_PREVENTIONALERT_RESPONSE,
    data
  }),
  clearPosts: () => ({
    type: preventionAlertActionsTypes.CLEAR_PREVENTIONALERT
  })
}

export const preventionAlertGetDetailsTypes = {
  GET_PREVENTION_ALERT_DETAILS: 'GET_PREVENTION_ALERT_DETAILS',
  PREVENTION_ALERT_DETAILS_RESPONSE: 'PREVENTION_ALERT_DETAILS_RESPONSE',
  CLEAR_PREVENTION_ALERT_DETAILS: 'CLEAR_PREVENTION_ALERT_DETAILS'
}

export const preventionAlertDetailsActions = {
  getPrevAlertDetails: (id) => ({
    type: preventionAlertGetDetailsTypes.GET_PREVENTION_ALERT_DETAILS,
    id
  }),
  savePrevAlertDetailsResponse: data => ({
    type: preventionAlertGetDetailsTypes.PREVENTION_ALERT_DETAILS_RESPONSE,
    data
  }),
  clearPrevAlertDetails: () => ({
    type: preventionAlertGetDetailsTypes.CLEAR_PREVENTION_ALERT_DETAILS
  })
}

export const editAlertsTypes = {
  REQUEST: 'EDIT_PREVENTIONALERT_REQUEST',
  RESPONSE: 'EDIT_PREVENTIONALERT_RESPONSE',
  ERROR: 'EDIT_PREVENTIONALERT_ERROR',
  CLEAR: 'EDIT_PREVENTIONALERT_CLEAR'
}

export const editAlertsActions = {
  editAlerts: (id, params) => {
    return {
      type: editAlertsTypes.REQUEST,
      payload: { id, params }
    }
  },
  saveeditAlertsResponse: (data) => {
    return {
      type: editAlertsTypes.RESPONSE,
      data
    }
  },
  cleareditAlerts: () => ({
    type: editAlertsTypes.CLEAR
  })
}
