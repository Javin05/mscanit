import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {
  preventionAlertActionsTypes,
  preventionAlertActions,
  preventionAlertGetDetailsTypes,
  preventionAlertDetailsActions,
  editAlertsTypes,
  editAlertsActions
} from '../actions'
import serviceList from '../../services/serviceList'

function * fetchPrevention (actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.preventionAlert, { params })
  if (json.status === 200) {
    yield put(preventionAlertActions.savePreventionAlertResponse(json.data))
  } else {
    yield put(preventionAlertActions.savePreventionAlertResponse([]))
  }
}

function * getPreventionDetails (actions) {
  const { id } = actions
  console.log(id);
  const endPointUrl = `${serviceList.preventionAlert}/${id}`
  const json = yield axiosInstance.get(endPointUrl)
  if (json.status === 200) {
    yield put(preventionAlertDetailsActions.savePrevAlertDetailsResponse(json.data))
  } else {
    yield put(preventionAlertDetailsActions.savePrevAlertDetailsResponse([]))
  }
}

function * editAlerts (actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.editAlerts}/${id}`
  const json = yield axiosInstance.put(endPointUrl, params)
  if (json.status === 200) {
    yield put(editAlertsActions.saveeditAlertsResponse(json.data))
  } else {
    yield put(editAlertsActions.saveeditAlertsResponse([]))
  }
}

export function * fetchPreventionWatcher () {
  yield all([
    yield takeLatest(preventionAlertActionsTypes.GET_PREVENTIONALERT, fetchPrevention),
    yield takeLatest(preventionAlertGetDetailsTypes.GET_PREVENTION_ALERT_DETAILS, getPreventionDetails),
    yield takeLatest(editAlertsTypes.REQUEST, editAlerts)
  ])
}
