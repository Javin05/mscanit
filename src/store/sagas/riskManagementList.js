import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import { riskManagementActions, riskManagementActionsTypes } from '../actions'
import serviceList from '../../services/serviceList'

function * fetchRiskmanagementlist (actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.riskManagementList, { params })
  if (json.status === 200) {
    yield put(riskManagementActions.saveRiskManagementlistResponse(json.data))
  } else {
    yield put(riskManagementActions.saveRiskManagementlistResponse([]))
  }
}

export function * fetchRiskmanagementlistWatcher () {
  yield all([
    yield takeLatest(riskManagementActionsTypes.GET_RISK_MANAGEMENT_LIST, fetchRiskmanagementlist)
  ])
}
