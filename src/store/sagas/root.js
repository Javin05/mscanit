import { all } from 'redux-saga/effects'
import { fetchPreventionWatcher } from './preventionAlert'
import { fetchLoginWatcher } from './login'
import { fetchForgotWatcher } from './forgotPassword'
import { fetchRiskmanagementlistWatcher } from './riskManagementList'

export default function * rootSaga () {
  yield all([
    fetchPreventionWatcher(),
    fetchLoginWatcher(),
    fetchForgotWatcher(),
    fetchRiskmanagementlistWatcher(),
  ])
}
