const API_URL = 'https://chargebackzero-userservice.herokuapp.com/api/v1'
const USER_SERVICES = 'https://chargebackzero-inquiryservice.herokuapp.com/api/v1'
const ISSUER_BANK = 'https://chargebackzero-issuerbankalert.herokuapp.com/api/v1'
const ALERT_SERVICE = 'https://chargebackzero-alertservice.herokuapp.com/api/v1'
const CHARGEBACK = 'https://chargebackzero-chargeback.herokuapp.com/api/v1'
const MERCHANT = 'https://irm-merchantservice.herokuapp.com/api/v1'
const serviceList = {
  merchantLogin: ` https://irm-merchantservice.herokuapp.com/api/v1/merchants`,
  login: `${API_URL}/user/login`,
  forgotPassword: `${API_URL}/user/forgotPassword`,
  verifyUser: `${API_URL}/user/checkForgotToken`,
  resetPassword: `${API_URL}/user/resetPassword`,
  posts: '/posts',
  quickSight: '/quicksight',
  chargeback: `${CHARGEBACK}/chargebacks`,
  addChargeback: `${CHARGEBACK}/chargebacks`,
  preventionAlert: `https://run.mocky.io/v3/e59aa90e-c8d6-443c-b442-c43d9fe239dc`,
  editAlerts: `${ALERT_SERVICE}/alerts`,
  orderinsight: `${USER_SERVICES}/inquiryAlerts`,
  export: 'https://chargebackzero-issuerbankalert.herokuapp.com/api/v1/issuerbankalerts?skipPagination=true',
  getClient: `${API_URL}/clients`,
  getPartner: `${API_URL}/partners`,
  getMerchant: `${API_URL}/merchants`,
  getBank: `${API_URL}/banks`,
  issuerbank: `${ISSUER_BANK}/issuerbankalerts`,
  riskManagementList: `${MERCHANT}/merchants`,
  issuerbankAlert: `${ISSUER_BANK}/issuerbankalerts/uploads`
}

export default serviceList
