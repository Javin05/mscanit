/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react/jsx-no-target-blank */
import React from 'react';
import { KTSVG } from '../../../helpers';
import { AsideMenuItemWithSub } from './AsideMenuItemWithSub';
import { AsideMenuItem } from './AsideMenuItem';

export function AsideMenuMain() {
  return (
    <>

      <AsideMenuItem
        to="/dashboard"
        icon="/media/svg-icons/dashboard.svg"
        title={'Dashboard'}
        fontIcon="bi-app-indicator"
      />

      <AsideMenuItem
        to='/create-own-market'
        title='Create your own market'
        fontIcon='bi-archive'
        icon='/media/svg-icons/marketing.svg'
      />

      <AsideMenuItem
        to="/location-settings"
        title="Location Settings"
        fontIcon="bi-archive"
        icon="/media/svg-icons/location.svg"
      />

      <AsideMenuItem
        to="/recurrence-settings"
        title="Recurrence Settings"
        fontIcon="bi-archive"
        icon="/media/svg-icons/recurrence.svg"
      />

      <AsideMenuItem
        to="/configuration"
        title="Configuration Sheet"
        fontIcon="bi-archive"
        icon="/media/svg-icons/configuration.svg"
      />

      <AsideMenuItem
        to="/mapping-table"
        title="Mapping Table"
        fontIcon="bi-archive"
        icon="/media/svg-icons/table.svg"
      />

      <AsideMenuItem
        to="/create-alert"
        title="Create Alert"
        fontIcon="bi-archive"
        icon="/media/svg-icons/alert.svg"
      />

      <AsideMenuItem
        to="/range-filter"
        title="Range Filter"
        fontIcon="bi-archive"
        icon="/media/svg-icons/filter.svg"
      />

      <AsideMenuItem
        to="/alert-group"
        title="Alert Mapping"
        fontIcon="bi-archive"
        icon="/media/svg-icons/mapping.svg"
      />

      <AsideMenuItem
        to="/reports"
        title="Reports"
        fontIcon="bi-archive"
        icon="/media/svg-icons/report.svg"
      />

      <AsideMenuItem
        to="/create-user"
        title="Create User"
        fontIcon="bi-archive"
        icon="/media/svg-icons/add-user.svg"
      />


    </>
  );
}
