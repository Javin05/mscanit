import React from 'react'
import Select from 'react-select'
import color from '../../../utils/colors'

function ReactSelect (props) {
  const { styles, options, handleChangeReactSelect, value, isMulti, isDisabled } = props

  return (
    <Select
      styles={styles}
      value={value || ''}
      onChange={(e) => handleChangeReactSelect(e)}
      options={options}
      theme={theme => ({
        ...theme,
        borderRadius: 0,
        borderWidth: 1,
        colors: {
          ...theme.colors,
          primary25: color.tangerine,
          primary: color.redSand,
          primary75: color.peach,
          primary50: color.redSand
        }
      })}
      isMulti={isMulti}
      isClearable
      isDisabled={isDisabled || false}
    />
  )
}

export default ReactSelect
