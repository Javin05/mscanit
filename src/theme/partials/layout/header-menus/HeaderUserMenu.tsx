/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { FC } from 'react';
import { shallowEqual, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { unsetLocalStorage } from '../../../../utils/helper';

const HeaderUserMenu: FC = () => {
  const dispatch = useDispatch();
  const logout = () => {
    // unsetLocalStorage()
    localStorage.removeItem("token");
    window.location.href = '/';
  };

  return (
    <div
      className="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg 
      menu-state-primary fw-bold py-4 fs-6 w-275px"
      data-kt-menu="true"
    >
      <div className="menu-item px-3">
        <div className="menu-content d-flex align-items-center px-3">
          <div className="symbol symbol-50px me-5">
            <img alt="Logo" src="/media/avatars/150-2.jpg" />
          </div>

          <div className="d-flex flex-column">
            <div className="fw-bolder d-flex align-items-center fs-5">
              David
            </div>
            <span className="fw-bold text-muted text-hover-primary fs-7">
              abc@gmail.com
            </span>
          </div>
        </div>
      </div>

      <div className="separator my-2" />

      <div className="menu-item px-5">
        <Link to='/myprofile' className="menu-link px-5">
          My Profile
        </Link>
      </div>

      <div className="menu-item px-5">
        <Link to='/changepassword' className="menu-link px-5">
          Change Password
        </Link>
      </div>

      <div className="separator my-2" />

      <div className="menu-item px-5">
        <a onClick={logout} className="menu-link px-5">
          Sign Out
        </a>
      </div>
    </div>
  );
};

export { HeaderUserMenu };
