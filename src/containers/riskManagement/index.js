import React from 'react'
import { Switch, Route } from 'react-router-dom'
import IssuerBankList from '../../components/issuerBank/issuerBank'
import { PageTitle } from '../../theme/layout/core'

const issuerBankBreadCrumbs = [
  {
    title: 'Risk Management',
    path: '/risk-management',
    isSeparator: false,
    isActive: false
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false
  }
]
function IssuerBank () {
  return (
    <Switch>
      <Route path='/risk-management'>
        <PageTitle breadcrumbs={issuerBankBreadCrumbs}>Risk Management</PageTitle>
        <IssuerBankList />
      </Route>
    </Switch>
  )
}

export default IssuerBank
