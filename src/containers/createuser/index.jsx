/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { TextField } from '../../shared-components/forms/TextField';
import { Formik, Form, Field } from "formik";
import SelectField from '../../shared-components/forms/Select';
import * as yup from "yup";
import { Row, Col, Button, Card } from "react-bootstrap";
import Breadcrumb from '../../shared-components/breadcrumb';
import ImageUploader from 'react-image-upload'
import 'react-image-upload/dist/index.css'

const initialValues = {
    username: '',
    email: '',
    phone_number: '',
    password: '',
    role: '',
    id: null
}

const role = [
    { value: "admin", label: "Admin" },
    { value: "customer", label: "Customer" },
]

const schema = yup.object().shape({
    username: yup.string().required(),
    email: yup.string().email().required(),
    phone_number: yup.number().required(),
    password: yup.string().required(),
    role: yup.string().required(),
});

function getImageFileObject(imageFile) {
    console.log({ imageFile })
}
function runAfterImageDelete(file) {
    console.log({ file })
}

export default function Createuser() {
    return (
        <>
            <Breadcrumb path="/create-user" labelname="Create New User" />
            <Card>
                <Card.Header>
                    <Card.Title>Create User</Card.Title>
                </Card.Header>
                <Card.Body>
                    <Formik
                        enableReinitialize={true}
                        validationSchema={schema}
                        initialValues={initialValues}
                        onSubmit={(values, { resetForm }) => {
                            resetForm();
                        }}
                    >
                        {({ values, setFieldValue, handleBlur }) => (
                            <Form>
                                <Row>
                                    <Col xs={12} sm={12} md={3} lg={3} xl={3}>
                                        <label>Add Logo</label>
                                        <ImageUploader
                                            onFileAdded={(img) => getImageFileObject(img)}
                                            onFileRemoved={(img) => runAfterImageDelete(img)}
                                        />
                                    </Col>

                                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                        <Row gutter={[20, 20]}>

                                            <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                                <Field name="username"
                                                    component={TextField}
                                                    placeholder="Enter Username"
                                                    label="Name"
                                                    type="text"
                                                />
                                            </Col>

                                            <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                                <Field name="email"
                                                    component={TextField}
                                                    placeholder="Enter Email ID"
                                                    label="Email"
                                                    type="email"
                                                />
                                            </Col>

                                            <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                                <Field name="phone_number"
                                                    component={TextField}
                                                    placeholder="Enter phone number"
                                                    label="Phone Number"
                                                    type="number"
                                                />
                                            </Col>

                                            <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                                <Field name="password"
                                                    component={TextField}
                                                    placeholder="Enter Password"
                                                    label="Password"
                                                    type="password"
                                                />
                                            </Col>


                                            <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                                <Field
                                                    component={SelectField}
                                                    name="role"
                                                    label="Role"
                                                    placeholder="Select Role"
                                                    option={role}
                                                    values={values}
                                                    setFieldValue={setFieldValue}
                                                    handleBlur={handleBlur}
                                                />
                                            </Col>

                                        </Row>

                                        <div className="text-center">
                                            <Button type="submit" className="w-100 mt-5">
                                                Create User
                                            </Button>
                                        </div>
                                    </Col>
                                </Row>



                            </Form>
                        )}
                    </Formik>
                </Card.Body>
            </Card>
        </>
    )
}
