import React from 'react'
import { Switch, Route } from 'react-router-dom'
import MerchantLogin from '../../components/merchantLogin/Login'
import { PageTitle } from '../../theme/layout/core'


function FraudAnalyzer () {
  return (
    <Switch>
      <Route path='/merchant-login'>
        <MerchantLogin />
      </Route>
    </Switch>
  )
}

export default FraudAnalyzer
