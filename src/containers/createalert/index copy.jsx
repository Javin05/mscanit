import React from 'react'
import { render } from "react-dom";
import Breadcrumb from "../../shared-components/breadcrumb";
 import MultiSelectField from '../../shared-components/forms/MultiSelect'
import  SelectField  from "../../shared-components/forms/Select";
import { TextField } from "../../shared-components/forms/TextField";
import { Formik, Form, Field, FieldArray } from "formik";
import { Row, Col, Button } from "react-bootstrap";

import * as yup from "yup";
import BootStrapForm from "react-bootstrap/Form";

export default function CreateAlert() {
    const schema = yup.object().shape({
     
        alert_name: yup.string().required(),
        alert_mapping:yup.string().required(),
   
        number:yup.string().required(),
        number_2:yup.string().required(),
      
        days_threshold:yup.string().required(),
        email_id:yup.string().required(),
        friends:yup.string().required()
     
  });
  const inputtype = {
    inputtype: [
      { value: "keyword", label: "Keyword" },
      { value: "urls", label: "URLs" },
      { value: "productcode", label: "Product Code" },
    ],
  };
  const alert_module = {
    alert_module: [
      { value: "1", label: "Brand1" },
      { value: "2", label: "Brand2" },
      { value: "3", label: "Brand3" },
      { value: "4", label: "Brand14" },
    ],
  };
    
    return (
        <div>

        <Breadcrumb path="/alert-group" labelname="Alert Group" />
         <Formik
               validationSchema={schema}
               initialValues={{
                alert_name: "",
                alert_mapping:"",
            
                number:'',
                number_2:'',
                days_threshold:'',
                email_id:'',
                friends:[""]
          
               }}
               onSubmit={(values) => {
                 // console.log(values);
                 alert(JSON.stringify(values, null, 2));
               }}
             >
               {({ errors, touched, values, setFieldValue }) => (
                 <Form>
                   <Row gutter={[20, 20, 20]}>
                     <Col xs={4} sm={4} md={4} lg={4} xl={4}>
                       <Field
                         name="alert_name"
                         component={TextField}
                         placeholder="Alert Name"
                         label="Alert Name"
                                                />
                     
                     </Col>
       
                     <Col xs={4} sm={4} md={4} lg={4} xl={4}>
                       <Field
                         name="alert_mapping"
                         component={TextField}
                         placeholder="Alert Mapping"
                         label="Alert Mapping"
                         
                       />
        
                     </Col>
                     <Col xs={4} sm={4} md={4} lg={4} xl={4}>
                       <Field
                         name="alert_module"
                         component={SelectField}
                         placeholder="Alert Module"
                         option={alert_module.alert_module}
                         label="Alert Module"
                         
                       />
                  
                     </Col>
                     <Col xs={4} sm={4} md={4} lg={4} xl={4}>
                       <Field
                         name="alert_type"
                         component={SelectField}
                         placeholder="Alert Type"
                         label="Alert Type"
                        option={alert_module.alert_module}
                         setFieldValue={setFieldValue}
                       />
              
                     </Col>
                     <Col xs={3} sm={3} md={3} lg={3} xl={3}>
                       <Field
                         name="threshold_condition"
                         component={SelectField}
                         placeholder="Own Threshold Condition"
                         label="Own Threshold Condition"
                        option={alert_module.alert_module}
                         setFieldValue={setFieldValue}
                       />
                  
                     </Col>
                     <Col xs={1} sm={1} md={1} lg={1} xl={1}>
                       <Field
                         name="number"
                         component={TextField}
                         placeholder="Number"
                         label="Number"

                       />
                   
                     </Col>
                     <Col xs={3} sm={3} md={3} lg={3} xl={3}>
                       <Field
                         name="competition_threshold_condition"
                         component={SelectField}
                         placeholder="Competition Threshold Condition"
                         label="Competition Threshold Condition"
                         option={alert_module.alert_module}
                         setFieldValue={setFieldValue}
                       />
                 
                     </Col>
                     <Col xs={1} sm={1} md={1} lg={1} xl={1}>
                       <Field
                         name="number_2"
                         component={TextField}
                         placeholder="Number"
                         label="Number"
               
                       />
                     
                     </Col>
                     <Col xs={4} sm={4} md={4} lg={4} xl={4}>
                       <Field
                         name="days_threshold"
                         component={TextField}
                         placeholder="Days Threshold"
                         label="Days Threshold"
                     
                       />
          
                     </Col>
                     <Col xs={4} sm={4} md={4} lg={4} xl={4}>
                       <Field
                         name="city"
                         component={MultiSelectField}
                         placeholder="City"
                         label="City"
                        option={alert_module.alert_module}
                         setFieldValue={setFieldValue}
                       />
                  
                     </Col>
                     <Col xs={4} sm={4} md={4} lg={4} xl={4}>
                       <Field
                         name="frequency"
                         component={SelectField}
                         placeholder="Frequency"
                         label="Frequency"
                        option={alert_module.alert_module}
                         setFieldValue={setFieldValue}
                       />
                  
                     </Col>
                     <Col xs={4} sm={4} md={4} lg={4} xl={4}>
                       <Field
                         name="contact_type"
                         component={SelectField}
                         placeholder="Contact Type"
                         label="Contact Type"
                         option={alert_module.alert_module}
                         setFieldValue={setFieldValue}
                       />
                  
                     </Col>
                    
                   </Row>
                   {/* {values.input_type.value === "urls" && ( */}
                    <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                      <FieldArray
                        name="friends"
                        render={(arrayHelpers) => (
                          <div className="fieldarray-ctr">
                            <label>Email ID</label>
                            {values.friends &&
                              values.friends.length > 0 &&
                              values.friends.map((friend, index) => (
                                <div key={index}>
                                  <Field
                                    name={`friends.${index}`}
                                    placeholder="Email"
                                  />
                                  {index === 0 ? (
                                    <span
                                      onClick={() => {
                                        if (friend !== "") {
                                         arrayHelpers.insert(index, "");
                                          setFieldValue={arrayHelpers}
                                        } else {
                                          alert("Alert");
                                        }
                                      }}
                                    >
                                      <i className="bi bi-plus-circle-fill"></i>
                                    </span>
                                  ) : (
                                    <i
                                      className="bi bi-dash-circle-fill"
                                      onClick={() => arrayHelpers.remove(index)}
                                    ></i>
                                  )}
                                </div>
                              ))}
                          </div>
                        )}
                      />
                    </Col>
                  {/* )} */}
          
                   <div className='d-flex justify-content-center'>
                   <Button type="submit" >
                           Save
                         </Button>
                         </div>
                 </Form>
               )}
             </Formik>
          
               </div>
    )
}
