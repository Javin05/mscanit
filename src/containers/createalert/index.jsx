import React, { useState, useEffect } from 'react';
import { TextField } from '../../shared-components/forms/TextField';
import { Formik, Form, Field, FieldArray } from "formik";
import SelectField from '../../shared-components/forms/Select';
import MultiSelectField from '../../shared-components/forms/MultiSelect';
import * as yup from "yup";
import { Row, Col, Button } from "react-bootstrap";
import Breadcrumb from '../../shared-components/breadcrumb';
import ConfirmModal from '../../shared-components/modal/confirmmodal';


const initialValues = {
    alert_name: '',
    alert_mapping: '',
    country: '',
    ecom_platforms: '',
    input_type: '',
    friends: [''],
    number: '',
    number2: '',
    id: null,
    days_threshold: '',
}

const country = [
    { value: "1", label: "India" },
    { value: "2", label: "America" },
    { value: "3", label: "China" },
    { value: "4", label: "Russia" }
]

const ecom = [
    { value: "1", label: "Ecom1" },
    { value: "2", label: "Ecom2" },
    { value: "3", label: "Ecom3" },
    { value: "4", label: "Ecom4" }
]

const contact_type = [
    { value: "email", label: "Email" },
    { value: "sms", label: "Sms" },
    { value: "whatsapp", label: "Whatsapp" },
]
const inputType = [
    { value: "keyword", label: "Keyword" },
    { value: "urls", label: "URLs" },
    { value: "productcode", label: "Product Code" },
]
const schema = yup.object().shape({
    alert_name: yup.string().required(),
    alert_mapping: yup.string().required(),
    input_type: yup.string().required(),
    number: yup.string().required(),
    number2: yup.string().required(),
});


export default function CreateAlert() {


    const [marketList, setMarketList] = useState([]);
    const [formValues, setFormData] = useState(initialValues);
    const [searchInput, setSearchInput] = useState(null);
    const [confirmModalShow, setConfirmModalShow] = useState(false);


    const handleEditMarket = (marketData) => {
        marketData.id = marketList.length + 1;
        setFormData(marketData);
    }

    const handleFilter = (searchInput) => {
        const marketListFilter =
            marketList.filter(item => Object.values(item).join('').toLowerCase().includes(searchInput.toLowerCase()));
        setMarketList(marketListFilter);
    }

    useEffect(() => {
        setTimeout(() => {
            handleFilter(searchInput);
        }, 500);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [searchInput]);


    const handleCreatUpdate = (formData, resetForm) => {
        if (formValues.id) {
            update(formData);
        } else {
            delete formData.id
            create(formData, resetForm);
        }
    }

    const update = (formData) => { }
    const create = (formData, resetForm) => { }

    const handleOnSubmit = (confirm) => {
        if (confirm) {
            console.log(formValues.id);
        }
    }

    return (
        <>
            <Breadcrumb path="/alert-group" labelname="Alert Group" />
            <Row>
                <Col xs={12} sm={12} md={12} lg={12} xl={4} className="card-box">
                    <div>
                        <Row className="align-items-center">
                            <Col>
                                <h3>LISTS</h3>
                            </Col>
                            <Col align="right">
                                <Button type="primary">
                                    Add New &nbsp;
                                    <i className="bi bi-plus-lg"></i>
                                </Button>
                            </Col>
                        </Row>
                        <div className="mb-5">
                            <input className="form-control" onChange={(e) => setSearchInput(e.target.value)} type="text" placeholder="Search..." />
                        </div>
                        <ul class="list-group">
                            {marketList && marketList.map((item) => (
                                <li className="list-group-item cursor-pointer" key={item.alert_name} onClick={() => handleEditMarket(item)}>{item.alert_name}</li>
                            ))}
                        </ul>
                    </div>
                </Col>

                <Col xs={12} sm={12} md={12} lg={12} xl={8}>
                    <div className="card-box">
                        <Formik
                            enableReinitialize={true}
                            validationSchema={schema}
                            initialValues={formValues}
                            onSubmit={(values, { resetForm }) => {
                                const marketLists = [...marketList, values]
                                setMarketList(marketLists);
                                resetForm();
                                handleCreatUpdate(values, resetForm);

                            }}
                        >
                            {({ values, setFieldValue, handleBlur }) => (
                                <Form>
                                    <Row gutter={[20, 20]}>
                                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                            <Field name="alert_name"
                                                component={TextField}
                                                placeholder="alert name"
                                                label="Alert Name"
                                            />
                                        </Col>

                                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                            <Field name="alert_mapping"
                                                component={TextField}
                                                placeholder="Alert Mapping"
                                                label="Alert Mapping" />
                                        </Col>

                                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                            <Field
                                                component={SelectField}
                                                name="alert_module"
                                                label="Alert Module"
                                                placeholder="Alert Module"
                                                option={country}
                                                setFieldValue={setFieldValue}
                                                values={values}
                                            />
                                        </Col>
                                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                            <Field
                                                component={SelectField}
                                                name="alert_type"
                                                label="Alert Type"
                                                placeholder="Alert Type"
                                                option={country}
                                                setFieldValue={setFieldValue}
                                                values={values}
                                            />
                                        </Col>
                                        <Col xs={12} sm={12} md={8} lg={8} xl={8}>
                                            <Field
                                                component={SelectField}
                                                name="threshold_condition"
                                                label="Threshold Condition"
                                                placeholder="Threshold Condition"
                                                option={country}
                                                setFieldValue={setFieldValue}
                                                values={values}
                                            />
                                        </Col>
                                        <Col xs={12} sm={12} md={4} lg={4} xl={4}>
                                            <Field name="number"
                                                component={TextField}
                                                placeholder="Number"
                                                label="Number" />
                                        </Col>
                                        <Col xs={12} sm={12} md={8} lg={8} xl={8}>
                                            <Field
                                                component={SelectField}
                                                name="competition_threshold_condition"
                                                label="Competition Threshold Condition"
                                                placeholder="Competition Threshold Condition"
                                                option={country}
                                                setFieldValue={setFieldValue}
                                                values={values}
                                            />
                                        </Col>
                                        <Col xs={12} sm={12} md={4} lg={4} xl={4}>
                                            <Field name="number2"
                                                component={TextField}
                                                placeholder="Number"
                                                label="Number" />
                                        </Col>
                                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                            <Field name="days_threshold"
                                                component={TextField}
                                                placeholder="Days Threshold"
                                                label="Days Threshold" />
                                        </Col>

                                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                            <Field
                                                component={MultiSelectField}
                                                name="city"
                                                label="City"
                                                placeholder="City"
                                                option={ecom}
                                                setFieldValue={setFieldValue}
                                            />
                                        </Col>

                                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                            <Field
                                                component={SelectField}
                                                name="frequency" label="Frequency"
                                                placeholder="Frequency"
                                                option={inputType}
                                                values={values}
                                                setFieldValue={setFieldValue}
                                                handleBlur={handleBlur}
                                            />
                                        </Col>
                                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                            <Field
                                                component={SelectField}
                                                name="contact_type" label="Contact Type"
                                                placeholder="Contact Type"
                                                option={inputType}
                                                values={values}
                                                setFieldValue={setFieldValue}
                                                handleBlur={handleBlur}
                                            />
                                        </Col>
                                        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                            <Field
                                                component={SelectField}
                                                name="input_type" label="Input Type"
                                                placeholder="Select Input Type"
                                                option={contact_type}
                                                values={values}
                                                setFieldValue={setFieldValue}
                                                handleBlur={handleBlur}
                                            />
                                        </Col>
                                    </Row>
                                    {values.input_type === "email" &&
                                        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                            <FieldArray
                                                name="friends"
                                                render={arrayHelpers => (
                                                    <div className="fieldarray-ctr">
                                                        <label>Email</label>
                                                        {values.friends && values.friends.length > 0 && (
                                                            values.friends.map((friend, index) => (
                                                                <div key={index}>
                                                                    <Field name={`friends.${index}`} placeholder="Enter Keywords" />
                                                                    {index === 0 ?
                                                                        <span onClick={() => {
                                                                            if (friend !== '') {
                                                                                arrayHelpers.insert(index, '')
                                                                            }
                                                                            else {
                                                                                alert("Alert");
                                                                            }
                                                                        }}>
                                                                            <i class="bi bi-plus-circle-fill"></i>
                                                                        </span>
                                                                        :
                                                                        <i class="bi bi-dash-circle-fill"
                                                                            onClick={() => arrayHelpers.remove(index)}></i>
                                                                    }
                                                                </div>
                                                            ))
                                                        )}

                                                    </div>
                                                )}
                                            />
                                        </Col>
                                    }

                                    {values.input_type === "sms" &&
                                        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                            <FieldArray
                                                name="friends"
                                                render={arrayHelpers => (
                                                    <div className="fieldarray-ctr">
                                                        <label>Sms</label>
                                                        {values.friends && values.friends.length > 0 && (
                                                            values.friends.map((friend, index) => (
                                                                <div key={index}>
                                                                    <Field name={`friends.${index}`} placeholder="SMS" />
                                                                    {index === 0 ?
                                                                        <span onClick={() => {
                                                                            if (friend !== '') {
                                                                                arrayHelpers.insert(index, '')
                                                                            }
                                                                            else {
                                                                                alert("Alert");
                                                                            }
                                                                        }}>
                                                                            <i class="bi bi-plus-circle-fill"></i>
                                                                        </span>
                                                                        :
                                                                        <i class="bi bi-dash-circle-fill"
                                                                            onClick={() => arrayHelpers.remove(index)}></i>
                                                                    }
                                                                </div>
                                                            ))
                                                        )}

                                                    </div>
                                                )}
                                            />
                                        </Col>
                                    }

                                    {values.input_type === "whatsapp" &&
                                        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                            <FieldArray
                                                name="friends"
                                                render={arrayHelpers => (
                                                    <div className="fieldarray-ctr">
                                                        <label>Whatsapp</label>
                                                        {values.friends && values.friends.length > 0 && (
                                                            values.friends.map((friend, index) => (
                                                                <div key={index}>
                                                                    <Field name={`friends.${index}`} placeholder="Enter Productcode" />
                                                                    {index === 0 ?
                                                                        <span onClick={() => {
                                                                            if (friend !== '') {
                                                                                arrayHelpers.insert(index, '')
                                                                            }
                                                                            else {
                                                                                alert("Alert");
                                                                            }
                                                                        }}>
                                                                            <i class="bi bi-plus-circle-fill"></i>
                                                                        </span>
                                                                        :
                                                                        <i class="bi bi-dash-circle-fill"
                                                                            onClick={() => arrayHelpers.remove(index)}></i>
                                                                    }
                                                                </div>
                                                            ))
                                                        )}

                                                    </div>
                                                )}
                                            />
                                        </Col>
                                    }

                                    <div className='d-flex justify-content-center'>
                                        <Button type="submit" >{formValues.id ? 'Update' : 'Submit'}</Button>
                                        {formValues.id && (
                                            <Button onClick={() => setConfirmModalShow(true)}> Delete </Button>
                                        )}
                                    </div>
                                </Form>
                            )}
                        </Formik>
                    </div>
                </Col>
            </Row>

            {confirmModalShow &&
                <ConfirmModal
                    show={confirmModalShow}
                    handleClose={() => setConfirmModalShow(false)}
                    handleSubmit={handleOnSubmit}
                    message="Do you want to delete this?"
                />
            }
        </>
    )
}
