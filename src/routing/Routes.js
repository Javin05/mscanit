import React from 'react'
import { Switch, Route, useLocation } from 'react-router-dom'
import { MasterLayout } from '../theme/layout/MasterLayout'
import { PrivateRoutes } from './PrivateRoutes'
import { PublicRoutes } from './PublicRoutes'
import { MasterInit } from '../theme/layout/MasterInit'
import { UnderConstruction } from '../containers/error'
import { PageTitle } from '../theme/layout/core'
import { SESSION } from '../utils/constants'
import { getLocalStorage } from '../utils/helper'
// import CreateCase from '../containers/createCase'
// import ImportCase from '../containers/createCase/import'

const Routes = () => {
  const isAuthorized = getLocalStorage(SESSION.TOKEN)
  const pathName = useLocation().pathname
  const search = useLocation().search
  const query = new URLSearchParams(search)
  const paramField = query.get('bank_id')
  console.log(getLocalStorage(SESSION.TOKEN))
  return (
    <>
      <Switch>
        {isAuthorized
          ? (
            <>
              <MasterLayout>
                <PrivateRoutes />
              </MasterLayout>
              )
            </>
          )
          : (
            <>
              <PublicRoutes />
            </>
          )}


        <MasterLayout>
          <PrivateRoutes />
        </MasterLayout>
      </Switch>

      <MasterInit />
    </>
  )
}

export { Routes }